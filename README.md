# Tutoring application backend

## Install dependencies

```$ npm install```

or

```$ yarn```

## Setting up your environment

* Create a new `.env` file in the root of the project
* Copy the contents of `.env.example` to your new `.env` file
* Fill in the fields of `.env` with usefull values

Current values that are mandatory for everyone to correctly run the backend code with the database are the following:

```
PORT=3000
DB_HOST=mysql-node
DB_USER=root
DB_PASSWORD=root
DB_DATABASE=root
```

## Running

### Run with NPM/yarn (recommended)
These scripts will automatically launch docker compose and initialize the database with examples.

```$ npm up```

or

```$ yarn up```

### Run with Docker
If the method described above doesn't work, you can also spin up the server by directly using the Docker api.

First, build the docker image

```$ docker build -t tutoring-backend .```

Then, run docker-compose to spin up all necessary containers

```$ docker-compose up```
