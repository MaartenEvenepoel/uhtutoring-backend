CREATE TABLE IF NOT EXISTS conversations (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS tutorConversation (
  id INT NOT NULL PRIMARY KEY,
  FOREIGN KEY (id) REFERENCES conversations(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS messages (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  senderID INT,
  conversationID INT,
  content VARCHAR(255) NOT NULL,
  timestamp DATETIME NOT NULL,
  readByReceiver BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (senderID) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (conversationID) REFERENCES conversations(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS conversationParticipants (
  conversationID INT NOT NULL,
  userID INT NOT NULL,
  FOREIGN KEY (conversationID) REFERENCES conversations(id) ON DELETE CASCADE,
  FOREIGN KEY (userID) REFERENCES users(id) ON DELETE CASCADE,
  PRIMARY KEY (conversationID, userID)
);

INSERT INTO conversations (name)
VALUES (NULL);
INSERT INTO conversations (name)
VALUES (NULL);
INSERT INTO conversations (name)
VALUES (NULL);
INSERT INTO conversationParticipants (conversationID, userID)
VALUES (1, 1);
INSERT INTO conversationParticipants (conversationID, userID)
VALUES (2, 1);
INSERT INTO conversationParticipants (conversationID, userID)
VALUES (1, 2);
INSERT INTO conversationParticipants (conversationID, userID)
VALUES (2, 3);
INSERT INTO messages (senderID, conversationID, content, timestamp, readByReceiver)
VALUES (1, 1, 'leuk bericht', '2019-11-11 01:23:45', TRUE);
INSERT INTO messages (senderID, conversationID, content, timestamp, readByReceiver)
VALUES (2, 1, 'wow', '2019-11-11 01:24:55', FALSE);
