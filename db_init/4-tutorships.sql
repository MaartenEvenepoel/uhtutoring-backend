CREATE TABLE IF NOT EXISTS tutorships (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tutorID int,
    tuteeID int,
    advertisementID int,
    creationTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (tuteeID) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (tutorID) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (advertisementID) REFERENCES advertisements(id) ON DELETE CASCADE,
    UNIQUE(tutorID, tuteeID, advertisementID)
);

-- INSERT INTO tutorships(tutorID, tuteeID, advertisementID, creationTimestamp, updateTimestamp)
-- VALUES (1, 2, 1, '2019-11-11 01:23:45', '2019-11-11 04:23:45');

