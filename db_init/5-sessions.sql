CREATE TABLE IF NOT EXISTS sessions (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tutorshipID int NOT NULL,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `subject` VARCHAR(200) NOT NULL,
    `description` VARCHAR(500) NOT NULL,
    creationTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (tutorshipID) REFERENCES tutorships(id) ON DELETE CASCADE,
    UNIQUE(tutorshipID, `date`)
);
