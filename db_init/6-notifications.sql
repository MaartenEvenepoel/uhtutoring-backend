CREATE TABLE IF NOT EXISTS notifications (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  receiverID INT,
  timestamp DATETIME NOT NULL,
  title VARCHAR(255) NOT NULL,
  content VARCHAR(255),
  url VARCHAR(255),
  readByReceiver BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (receiverID) REFERENCES users(id) ON DELETE CASCADE
);
INSERT INTO notifications (receiverID, timestamp, title, content, url)
VALUES (1, '2019-11-11 01:23:45', 'melding titel', 'dit is een voorbeeld melding', 'rms.sexy');
