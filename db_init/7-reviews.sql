CREATE TABLE IF NOT EXISTS reviews (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  tutorID INT,
  senderID INT,
  timestamp DATETIME NOT NULL,
  communication VARCHAR(255),
  organization VARCHAR(255),
  content VARCHAR(255) NOT NULL,
  extra VARCHAR(255),
  FOREIGN KEY (tutorID) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (senderID) REFERENCES users(id) ON DELETE CASCADE
);
INSERT INTO reviews (tutorID, senderID, timestamp, communication, organization, content)
VALUES (2, 1, '2019-11-11 01:23:45', 'geen communicatie', 'slechte organisatie', 'Deze tutor is een faggot.');
