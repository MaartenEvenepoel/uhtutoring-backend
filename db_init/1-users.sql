GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root';

CREATE TABLE IF NOT EXISTS programmes (
  `name` VARCHAR(100) NOT NULL UNIQUE PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS users (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  firstname VARCHAR(255) NOT NULL,
  lastname VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  passHash VARCHAR(400) NOT NULL,
  role ENUM('tutee', 'tutor', 'mentor', 'admin') NOT NULL,
  programme VARCHAR(100) NOT NULL,
  FOREIGN KEY (programme) REFERENCES programmes(name)
);

CREATE TABLE IF NOT EXISTS registrationHashes (
  email varchar(255) NOT NULL UNIQUE PRIMARY KEY,
  `hash` VARCHAR(255) NOT NULL UNIQUE,
  FOREIGN KEY (email) REFERENCES users(email) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS userTokens (
  userID int NOT NULL UNIQUE PRIMARY KEY,
  token VARCHAR(400) NOT NULL UNIQUE,
  FOREIGN KEY (userID) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS confirmedEmails (
  email VARCHAR(255) NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS pendingTutors (
  id int NOT NULL UNIQUE,
  motivation VARCHAR(500) NOT NULL,
  FOREIGN KEY (id) REFERENCES users(id) ON DELETE CASCADE
);

INSERT INTO programmes (name) VALUES ('informatica');

-- INSERT INTO users (firstname, lastname, email, passHash, role, programme)
-- VALUES ('Maarten', 'Evenepoel', 'maarten.evenepoel@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'admin', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('Emile', 'Peetermans', 'emile.peetermans@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'admin', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('Veerle', 'Smeers', 'veerle.smeers@uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'mentor', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutee1a', 'tutee1b', 'tutee1@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutee', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutee2a', 'tutee2b', 'tutee2@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutee', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutee3a', 'tutee3b', 'tutee3@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutee', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutee4a', 'tutee4b', 'tutee4@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutee', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutor1a', 'tutor1b', 'tutor1@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutor', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutor2a', 'tutor2b', 'tutor2@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutor', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutor3a', 'tutor3b', 'tutor3@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutor', 'informatica');
INSERT INTO users (firstname, lastname, email, passHash, role, programme)
VALUES ('tutor4a', 'tutor4b', 'tutor4@student.uhasselt.be', 'sha1$5f75425c$1$c9db13bb6e3f63a4b19117c6f081431155e76810', 'tutor', 'informatica');

INSERT INTO userTokens (userID, token)
VALUES (1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs");
