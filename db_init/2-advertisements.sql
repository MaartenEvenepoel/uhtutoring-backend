CREATE TABLE IF NOT EXISTS advertisements (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  `description` VARCHAR(1024) NOT NULL,
  tuteeID int,
  programme VARCHAR(100) NOT NULL,
  creationTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updateTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (tuteeID) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (programme) REFERENCES programmes(name) ON DELETE CASCADE
);

-- INSERT INTO advertisements (title, description, tuteeID, creationTimestamp, updateTimestamp)
-- VALUES ('wow ad', 'nice advertisement', 2, '2019-11-11 01:23:45', '2019-11-11 01:23:45');
