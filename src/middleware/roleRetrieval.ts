import { NextFunction, Request, Response } from "express";
import jwt, { JsonWebTokenError } from "jsonwebtoken";
import { MysqlError } from "mysql";
import { database } from "../database/init";

export function getRole(req: Request, res: Response, next: NextFunction ) {

    database.query("SELECT users.role FROM users, userTokens WHERE userTokens.userID=users.id AND userTokens.token=?", [
        req.token,
    ], (err: MysqlError | null, results: any) => {

        if (err) {
            return res.status(500).json({ msg: err });
        }

        req.body.role = results[0].role;

        next();
    });

}
