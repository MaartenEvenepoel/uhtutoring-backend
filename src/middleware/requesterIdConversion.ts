import { NextFunction, Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../database/init";

export function tokenToId(req: Request, res: Response, next: NextFunction ) {
  database.query("SELECT userID from userTokens WHERE token = ?;", [
    req.token,
  ], (err: MysqlError | null, results: any) => {
    if (err) {
      return res.status(401).json({ msg: err });
    }
    req.body.userID = results[0].userID;
    req.userID = results[0].userID;
    next();
  });
}
