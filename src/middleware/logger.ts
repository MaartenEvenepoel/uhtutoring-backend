import { NextFunction, Request, Response } from "express";
import moment = require("moment");

// Logger middleware to log the performed HTTP request and the timestamp it happened
const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log(
        `${req.method} ${req.originalUrl} ${moment().toDate()}`,
    );
    next();
};

export default logger;
