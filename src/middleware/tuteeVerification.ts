import { NextFunction, Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../database/init";

export async function verifyTutee(req: Request, res: Response, next: NextFunction ) {

    await database.query("SELECT users.role FROM users, userTokens WHERE userTokens.token=? AND userTokens.userID=users.id", [
        req.token,
    ], (err: MysqlError | null, results: any) => {

        if (err) {
            return res.status(400).json({ msg: "Invalid request"});
        }

        const role: string = results[0].role;

        if (role !== "tutee") {
            return res.status(403).json({ msg: "Request only allowed for tutee's" });
        }

        next();
    });
}
