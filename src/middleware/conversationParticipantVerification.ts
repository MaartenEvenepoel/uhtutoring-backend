import { NextFunction, Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../database/init";

export function checkConversationParticipation(req: Request, res: Response, next: NextFunction ) {
  database.query("SELECT * FROM conversationParticipants WHERE userID = ? AND conversationID = ?;", [
    req.body.userID,
    req.params.conversationID,
  ], (err: MysqlError | null, results: any) => {
    if (err) {
      throw err;
    }
    if (results.length === 0) {
      return res.status(401).json({ msg: "User is not a part of this conversation." });
    }
    next();
  });

}
