import { NextFunction, Request, Response } from "express";
import jwt, { JsonWebTokenError } from "jsonwebtoken";
import { MysqlError } from "mysql";
import { database } from "../database/init";

export function verifyToken(req: Request, res: Response, next: NextFunction ) {

    // Get auth header value
    const bearerHeader = req.headers.authorization;

    // Check if bearer is undefined
    if (typeof bearerHeader !== "undefined") {
        const token = bearerHeader.split(" ")[1];

        database.query("SELECT * FROM userTokens WHERE token=?", [
            token,
        ], (err: MysqlError | null, results: any) => {
            if (err) {
                throw err;
            }
            if (results.length === 0) {
                return res.status(403).json({ msg: "Unauthorized, token does not exist" });
            }

            jwt.verify(token, `${process.env.SECRET_KEY}`, (jwtErr: JsonWebTokenError, authData: any) => {
                if (jwtErr) {
                    return res.status(403).json({ msg: "Unauthorized, invalid token" });
                } else {
                    req.token = token;
                    next();
                }
            });
        });
    } else {
        // Forbidden
        return res.status(401).json({ msg: "Unauthenticated, no token" });
    }
}
