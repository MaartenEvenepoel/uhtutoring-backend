import dotenv from "dotenv-safe";

function loadEnvironment() {
    dotenv.config();
}

export { loadEnvironment };
