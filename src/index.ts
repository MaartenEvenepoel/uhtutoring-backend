import cron from "node-cron";
import { app } from "./api";
import { loadEnvironment } from "./config/config";
import { database, initDatabase } from "./database/init";
import { sendNotificationForOpenAdvertisements } from "./util/notificationsUtils";
import { WebSocketServer } from "./websocket/server";

let wss: WebSocketServer;

(async () => {
    loadEnvironment();
    initDatabase();
    const port = process.env.PORT;
    // Host listening socket
    const server = app.listen(port, () => {
        console.log(`Server listening on port ${port}`);
    });

    wss = new WebSocketServer(server);

    // Execute every day at 0:00
    cron.schedule("0 0 * * *", () => {
      sendNotificationForOpenAdvertisements();
    });

    /*
    For production server to keep db connection alive

    setInterval(() => {
      database.query("SELECT * FROM users WHERE id = 1;",
      (err: any, results: any, fields: any) => {
        if (err) {
          console.log(err);
        }
      });
    }, 60000);
    */
})().catch((err) => {
    console.log(err);
});

export { wss };
