// import dotenv from "dotenv-safe";
// dotenv.config();
import { loadEnvironment } from "../config/config";

import mysql from "mysql";

loadEnvironment();

const dbhost = process.env.DB_HOST;
const dbuser = process.env.DB_USER;
const dbpassword = process.env.DB_PASSWORD;
const dbdatabase = process.env.DB_DATABASE;

const database = mysql.createConnection({
    host: dbhost,
    user: dbuser,
    password: dbpassword,
    database: dbdatabase,
});

function initDatabase() {
    database.connect((err) => {
        if (err) {
            throw err;
        } else {
            console.log("Succesfully connected to database");
        }
    });
}

export { database, initDatabase };
