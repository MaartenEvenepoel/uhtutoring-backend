import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../../database/init";
import { getProgramme } from "../../middleware/programmeretrieval";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { getRole } from "../../middleware/roleRetrieval";
import { verifyToken } from "../../middleware/tokenVerification";
import { verifyTutee } from "../../middleware/tuteeVerification";

export const advertisementsRouter = express.Router();

advertisementsRouter.post("/", verifyToken, verifyTutee, tokenToId, getProgramme, (req: Request, res: Response ) => {

    database.query("INSERT INTO advertisements (title, description, tuteeID, programme) VALUES (?, ?, ?, ?)", [
        req.body.title,
        req.body.description,
        req.body.userID,
        req.body.programme,
    ], (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(400).json({ msg: err });
        } else {
            return res.status(201).json({
                msg: "Succesfully created advertisement",
            });
        }
    });

});

advertisementsRouter.get("/", verifyToken, tokenToId, getRole, (req: Request, res: Response) => {

    if (req.body.role === "mentor") {

        const query: string = "SELECT * FROM advertisements";

        database.query(query, (err: MysqlError | null, results: any) => {

            if (err) {
                return res.status(500).json({ msg: err });
            }

            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried advertisements",
                data,
            });
        });

    } else if (req.body.role === "tutor") {

        const query: string = "SELECT * FROM advertisements WHERE id NOT IN (SELECT advertisementID AS id FROM tutorships)";

        database.query(query, (err: MysqlError | null, results: any) => {

            if (err) {
                return res.status(500).json({ msg: "Unable to retrieve advertisements from database" });
            }

            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried advertisements",
                data,
            });
        });

    } else if (req.body.role === "tutee") {

        database.query("SELECT * FROM advertisements WHERE tuteeID=?", [
            req.body.userID,
        ], (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: "Unable to retrieve advertisements from database" });
            }

            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried advertisements",
                data,
            });
        });

    }
});

advertisementsRouter.put("/:id", verifyToken, verifyTutee, tokenToId, (req: Request, res: Response) => {

    database.query("UPDATE advertisements SET title=?, description=? WHERE tuteeID=? AND id=?", [
        req.body.title,
        req.body.description,
        req.body.userID,
        req.params.id,
    ], (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        } else {
            return res.status(200).json({ msg: "Succesfully updated advertisement" });
        }
    });

});

advertisementsRouter.delete("/:id", verifyToken, verifyTutee, tokenToId, (req: Request, res: Response) => {

    database.query("DELETE FROM advertisements WHERE tuteeID=? AND id=?", [
        req.body.userID,
        req.params.id,
    ], (err: MysqlError | null, results: any) => {

        if (err) {
            return res.status(500).json({ msg: err });
        } else {
            return res.status(200).json({
                msg: "Succesfull delete",
                data: {
                    recordsAffected: results.affectedRows,
                },
            });
        }
    });

});
