import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../../database/init";
import { verifyAdmin } from "../../middleware/adminVerification";
import { verifyToken } from "../../middleware/tokenVerification";

export const programmesRouter = express.Router();

programmesRouter.post("/", verifyToken, verifyAdmin, (req: Request, res: Response ) => {

    database.query("INSERT INTO programmes(name) VALUES (?)", [
        req.body.programme,
    ], (err: MysqlError | null) => {
        if (err) {
            return res.status(500).json({ msg: err });
        } else {
            return res.status(201).json({ msg: "Succesfully created new programme" });
        }
    });

});

programmesRouter.get("/", (req: Request, res: Response) => {

    database.query("SELECT * FROM programmes", (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        }

        const data = JSON.parse(JSON.stringify(results));

        return res.status(200).json({
            msg: "Succesfully retrieved all programmes",
            data,
        });
    });

});
