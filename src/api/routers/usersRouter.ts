import express, { Request, Response } from "express";
import jwt from "jsonwebtoken";
import { MysqlError } from "mysql";
import passwordHasher from "password-hash";
import { database } from "../../database/init";
import { mailTemplate } from "../../mail/declarations.ts/mailTemplate";
import { sendMail } from "../../mail/mail";
import { verifyAdmin } from "../../middleware/adminVerification";
import { verifyMentor } from "../../middleware/mentorVerification";
import { getRole } from "../../middleware/roleRetrieval";
import { verifyToken } from "../../middleware/tokenVerification";
import { buildAcceptanceMailContent, buildConfirmationMailContent, buildRejectionMailContent } from "../../util/mailUtils";
import { addUserToTutorConversation } from "../../util/messagesUtils";
import { INotification, sendNotificationToRoles } from "../../util/notificationsUtils";
import { createAndStoreRegistrationHash } from "../../util/usersUtils";

export const usersRouter = express.Router();

/**
 * Expected body format:
 * "email": "john.doe@student.uhasselt.be",
 * "password": "mysupersecretpassword"
 */
usersRouter.post("/login", (req: Request, res: Response) => {

    // Fetch user from database
    database.query("SELECT * FROM users WHERE email=?", [
        req.body.email,
    ], (err: MysqlError | null, results?: any) => {

        if (err) {
            return res.status(500).json({ msg: err });
        }

        // Check if user exists
        if (results.length === 0) {
            return res.status(400).json({ msg: "User doesn't exist" });
        }

        // Serialize user
        const user = JSON.parse(JSON.stringify(results[0]));

        database.query("SELECT * FROM pendingTutors WHERE id=?", [user.id], (pendingTutorErr: MysqlError | null, pendingTutor: any) => {
            if (pendingTutorErr) {
                return res.status(500).json({ msg: pendingTutorErr });
            }

            if (pendingTutor.length !== 0) {
                return res.status(403).json({ msg: "Unverified tutor" });
            } else {
                // Check password
                if (passwordHasher.verify(req.body.password, user.passHash)) {

                    // Sign a json webtoken
                    jwt.sign({user}, `${process.env.SECRET_KEY}`, (jwterr: Error, token: string) => {

                        if (jwterr) {
                            throw jwterr;
                        }

                        database.query("INSERT INTO userTokens (userID, token) VALUES (?, ?) ON DUPLICATE KEY UPDATE token=VALUES(token)", [
                            user.id,
                            token,
                        ], (insertErr: MysqlError | null) => {
                            if (insertErr) {
                                throw insertErr;
                            }
                        });

                        // Remove passhash from return object
                        delete user.passHash;

                        return res.status(200).json({
                            msg: "Login succesfull",
                            data: {
                                user: {
                                    ...user,
                                },
                                token,
                            },
                        });
                    });
                } else {
                    return res.status(400).json({ msg: "Password incorrect" });
                }
            }
        });
    });
});

/**
 * Expected body format:
 * "firstname": "John",
 * "lastname": "Doe",
 * "email": "john.doe@student.uhasselt.be",
 * "password": "mysupersecretpassword",
 * "programme": "informatica",
 * "role": "tutee"
 */
usersRouter.post("/register", (req: Request, res: Response) => {

    // Hash password
    const hashedPassword: string = passwordHasher.generate(req.body.password);

    // Verify UHasselt student
    const parts = req.body.email.split("@");
    if (parts[1] !== "student.uhasselt.be") {
        return res.status(403).json({ msg: "No UHasselt email" });
    }

    // Verify role is not admin or mentor
    if (req.body.role !== "tutee" && req.body.role !== "tutor") {
        return res.status(403).json({ msg: "Role not allowed" });
    }

    // Create new user
    database.query("INSERT INTO users (firstname, lastname, email, passHash, role, programme) values (?,?,?,?,?,?)", [
        req.body.firstname,
        req.body.lastname,
        req.body.email,
        hashedPassword,
        req.body.role,
        req.body.programme,
    ], async (err: MysqlError | null, results: any) => {
        if (err) {
            if (err.code === "ER_DUP_ENTRY") {
                return res.status(400).json({ msg: "User already exists" });
            } else {
                return res.status(500).json({ msg: err });
            }
        } else {
            if (req.body.role === "tutor") {
                database.query("INSERT INTO pendingTutors (id, motivation) values (?, ?)", [
                    results.insertId,
                    req.body.motivation,
                ], async (tutorErr: MysqlError | null) => {
                    if (tutorErr) {
                        return res.status(500).json({ msg: tutorErr });
                    } else {
                        const registrationHash: string = await createAndStoreRegistrationHash(req.body.email, new Date().toString());
                        const mailcontent: string = await buildConfirmationMailContent(req.body, registrationHash);
                        sendMail(req.body.email, mailTemplate.emailconfirmation, mailcontent);
                        sendNotificationToRoles(["mentor"], {title: "New tutor registration", content: "A new student has applied to become a tutor", url: ""});

                        return res.status(201).json({ msg: "Registration succesfull"});
                    }
                });
            } else {
                const registrationHash: string = await createAndStoreRegistrationHash(req.body.email, new Date().toString());
                const mailcontent: string = await buildConfirmationMailContent(req.body, registrationHash);
                sendMail(req.body.email, mailTemplate.emailconfirmation, mailcontent);
                return res.status(201).json({ msg: "Registration succesfull" });
            }
        }
    });
});

usersRouter.post("/register/mentor", verifyToken, verifyAdmin, (req: Request, res: Response) => {

    // Hash password
    const hashedPassword: string = passwordHasher.generate(req.body.password);

    // Verify UHasselt student
    const parts = req.body.email.split("@");
    if (parts[1] !== "uhasselt.be") {
        return res.status(403).json({ msg: "No UHasselt email" });
    }

    // Verify role is not admin or mentor
    if (req.body.role !== "mentor") {
        return res.status(403).json({ msg: "Role not allowed" });
    }

    // Create new user
    database.query("INSERT INTO users (firstname, lastname, email, passHash, role, programme) values (?,?,?,?,?,?)", [
        req.body.firstname,
        req.body.lastname,
        req.body.email,
        hashedPassword,
        req.body.role,
        req.body.programme,
    ], (err: MysqlError | null, results: any) => {
        if (err) {
            if (err.code === "ER_DUP_ENTRY") {
                return res.status(400).json({ msg: "User already exists" });
            } else {
                return res.status(500).json({ msg: err });
            }
        } else {
            return res.status(201).json({ msg: "Registration succesfull" });
        }
    });

});

/**
 * Expected body format:
 * "hash": "skjdfhgwjke34iukhtk34wiy348iie8yg3i84"
 */
usersRouter.post("/confirm", (req: Request, res: Response) => {
    // Fetch user from database
    database.query("SELECT * FROM registrationHashes WHERE hash=?", [
        req.body.hash,
    ], (err: MysqlError | null, results: any) => {

        if (err) {
            return res.status(500).json({ msg: err });
        }

        // Check if user exists
        if (results.length === 0) {
            return res.status(400).json({ msg: "Invalid hash" });
        }

        // Serialize record
        const record = JSON.parse(JSON.stringify(results[0]));

        database.query("DELETE FROM registrationHashes WHERE hash=?", [
            req.body.hash,
        ]);

        database.query("INSERT INTO confirmedEmails (email) values (?)", [
            record.email,
        ], (insertErr: any) => {
            if (insertErr) {
                if (insertErr.code === "ER_DUP_ENTRY") {
                    return res.status(400).json({ msg: "Email already confirmed" });
                } else {
                    throw err;
                }
            } else {
                return res.status(200).json({ msg: "Email confirmed" });
            }
        });
    });
});

usersRouter.post("/tutors/accept/:id", verifyToken, verifyMentor, (req: Request, res: Response) => {
    database.query("DELETE FROM pendingTutors WHERE id=?", [
        req.params.id,
    ], async (err: MysqlError | null, results: any) => {
        if (err) {
          return res.status(500).json({ msg: err });
        } else {

            database.query("SELECT * FROM users WHERE id=?", [
                req.params.id,
            ], async (idFetchErr: MysqlError | null, users: any) => {

                const data = JSON.parse(JSON.stringify(users[0]));

                const mailcontent: string = await buildAcceptanceMailContent(data, req.body.message);
                sendMail(data.email, mailTemplate.tutorAcceptance, mailcontent);
            });

            addUserToTutorConversation(res, Number(req.params.id), () => {
                return res.status(200).json({
                    msg: "Succesfully allowed tutor to the system",
                    data: {
                        recordsAffected: results.affectedRows,
                    },
                });
            });
        }
    });
});

usersRouter.post("/tutors/refuse/:id", verifyToken, verifyMentor, async (req: Request, res: Response) => {

    database.query("DELETE FROM pendingTutors WHERE id=?", [
        req.params.id,
    ], (err: MysqlError | null) => {
        if (err) {
            return res.status(500).json({ msg: err });
        } else {
            database.query("DELETE FROM users WHERE id=?", [
                req.params.id,
            ], (IDerr: MysqlError | null) => {
                if (err) {
                    return res.status(500).json({ msg: err });
                } else {
                    return res.status(200).json({ msg: "Succesfully refused tutor"});
                }
            });
        }
    });

    database.query("SELECT * FROM users WHERE id=?", [
        req.params.id,
    ], async (err: MysqlError | null, users: any) => {
        if (users.length > 0) {
            const data = JSON.parse(JSON.stringify(users[0]));
            const mailcontent: string = await buildRejectionMailContent(data, req.body.message);
            sendMail(data.email, mailTemplate.tutorRejection, mailcontent);
        }
    });
});

usersRouter.get("/tutors/pending", verifyToken, verifyMentor, (req: Request, res: Response) => {

    database.query("SELECT users.id, users.firstname, users.lastname, users.email, pendingTutors.motivation FROM users, pendingTutors WHERE users.id=pendingTutors.id", (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        }

        const data = JSON.parse(JSON.stringify(results));

        return res.status(200).json({
            msg: "Succesfully retrieved all pending tutor applications",
            data,
        });
    });

});

usersRouter.get("/tutors", verifyToken, verifyMentor, (req: Request, res: Response) => {

    database.query("SELECT id, firstname, lastname, email, programme FROM users WHERE role='tutor'", (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        }

        const data = JSON.parse(JSON.stringify(results));

        return res.status(200).json({
            msg: "Succesfully retrieved all tutors",
            data,
        });
    });

});

usersRouter.delete("/:deleteID", verifyToken, verifyMentor, (req: Request, res: Response) => {

    database.query("DELETE FROM users WHERE id = ?", [
        req.params.deleteID,
    ], (err: MysqlError | null, results: any) => {
        if (err) {
            console.log(err);
            return res.status(400).json({ msg: "Bad request" });
        }
        return res.status(204).json({
            msg: "Succesfully deleted user",
        });
    });
});

usersRouter.get("/students", verifyToken, getRole, (req: Request, res: Response) => {

    if (req.body.role !== "admin" && req.body.role !== "mentor") {
        return res.status(403).json({ msg: "Endpoint only allowed for admins or mentors" });
    }

    database.query("SELECT id, firstname, lastname, email, role, programme FROM users WHERE role!='mentor' && role!='admin'", (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        }

        const data = JSON.parse(JSON.stringify(results));

        return res.status(200).json({
            msg: "Succesfully retrieved all students",
            data,
        });
    });
});

usersRouter.get("/mentors", verifyToken, verifyAdmin, (req: Request, res: Response) => {

    database.query("SELECT id, firstname, lastname, email, role, programme FROM users WHERE role='mentor'", (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        }

        const data = JSON.parse(JSON.stringify(results));

        return res.status(200).json({
            msg: "Succesfully retrieved all mentors",
            data,
        });
    });

});
