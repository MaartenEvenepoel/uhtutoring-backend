import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../../database/init";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { getRole } from "../../middleware/roleRetrieval";
import { verifyToken } from "../../middleware/tokenVerification";
import { verifyTutor } from "../../middleware/tutorVerification";

export const sessionsRouter = express.Router();

sessionsRouter.post("/", verifyToken, verifyTutor, (req: Request, res: Response ) => {

    database.query("INSERT INTO sessions(tutorshipID, date, subject, description) VALUES (?, ?, ?, ?)", [
        req.body.tutorshipID,
        req.body.date,
        req.body.subject,
        req.body.description,
    ], (err: MysqlError | null) => {
        if (err) {
            if (err.code === "ER_DUP_ENTRY") {
                return res.status(400).json({ msg: "Session already exists" });
            } else {
                return res.status(500).json({ msg: err });
            }
        } else {
            return res.status(201).json({ msg: "Succesfully created session" });
        }
    });

});

sessionsRouter.put("/:id", verifyToken, getRole, tokenToId, (req: Request, res: Response) => {

    database.query("UPDATE sessions SET subject=?, date=?, description=? WHERE id=? AND tutorshipID IN (SELECT id AS tutorshipID FROM tutorships WHERE tuteeID=? OR tutorID=?)", [
        req.body.subject,
        req.body.date,
        req.body.description,
        req.params.id,
        req.body.userID,
        req.body.userID,
    ], (err: MysqlError | null, results: any) => {

        if (err) {
            return res.status(500).json({ msg: err });
        }

        return res.status(200).json({ msg: "Succesfully updated session details" });
    });

});

sessionsRouter.get("/", verifyToken, getRole, tokenToId, (req: Request, res: Response) => {

    if (req.body.role === "mentor") {

        database.query("SELECT * FROM sessions", (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: err });
            }

            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried sessions",
                data,
            });
        });

    } else if (req.body.role === "tutor") {

        database.query("SELECT * FROM sessions WHERE tutorshipID IN (SELECT id AS tutorshipID FROM tutorships WHERE tutorID=?)", [
            req.body.userID,
        ], (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: err });
            }
            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried sessions",
                data,
            });
        });

    } else if (req.body.role === "tutee") {

        database.query("SELECT * FROM sessions WHERE tutorshipID IN (SELECT id AS tutorshipID FROM tutorships WHERE tuteeID=?)", [
            req.body.userID,
        ], (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: err });
            }
            const data = JSON.parse(JSON.stringify(results));

            return res.status(200).json({
                msg: "Succesfully queried sessions",
                data,
            });
        });
    }

});
