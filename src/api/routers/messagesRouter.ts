import express, { Request, Response } from "express";
import jwt, { JsonWebTokenError } from "jsonwebtoken";
import { database } from "../../database/init";
import { wss } from "../../index";
import { checkConversationParticipation } from "../../middleware/conversationParticipantVerification";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { verifyToken } from "../../middleware/tokenVerification";
import { addUserToConversation, createConversation } from "../../util/messagesUtils";
import { getUsersFromTutorship } from "../../util/usersUtils";

export const messagesRouter = express.Router();

messagesRouter.get("/conversations", verifyToken, tokenToId,
  (req: Request, res: Response) => {
  database.query("SELECT conversations.id, IFNULL(conversations.name, "
  + "(SELECT users.firstname FROM users WHERE users.id != ? AND users.id IN (SELECT cp.userID FROM conversationParticipants AS cp WHERE cp.conversationID = conversations.id) limit 1)"
  + ") AS 'name' FROM conversationParticipants INNER JOIN conversations ON conversationParticipants.conversationID = conversations.id WHERE conversationParticipants.userID = ?;", [
      req.body.userID,
      req.body.userID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
                msg: "Bad request",
        });
      }
      const conversations = JSON.parse(JSON.stringify(results));
      return res.status(200).json({
              msg: "Conversations get",
              conversations,
      });
  });
});

messagesRouter.post("/conversation/:tutorshipID", verifyToken, tokenToId,
  (req: Request, res: Response) => {
  getUsersFromTutorship(res, +req.params.tutorshipID, (tutorID: number, tuteeID: number) => {
    createConversation(res, (conversationID: number) => {
      addUserToConversation(res, conversationID, tuteeID, () => {
        addUserToConversation(res, conversationID, tutorID, () => {
          return res.status(201).json({
                  msg: "Conversation was sucessfully created",
                  id: conversationID,
          });
        });
      });
    });
  });
});

messagesRouter.get("/:conversationID", verifyToken, tokenToId,
  checkConversationParticipation, (req: Request, res: Response) => {
  database.query("SELECT messages.id, messages.content, messages.timestamp, users.firstname AS 'sender', messages.readByReceiver, (messages.senderID = ?) AS 'sentByCurrentUser' FROM messages "
  + "INNER JOIN users ON users.id = messages.senderID WHERE messages.conversationID = ?;", [
      req.body.userID,
      req.params.conversationID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
                msg: "Bad request",
        });
      }
      const messages = JSON.parse(JSON.stringify(results));
      return res.status(200).json({
              msg: "Messages get",
              messages,
      });
  });
});

messagesRouter.post("/:conversationID", verifyToken, tokenToId,
  checkConversationParticipation, (req: Request, res: Response) => {
  database.query("INSERT INTO messages (senderID, conversationID, content, timestamp, readByReceiver) VALUES (?, ?, ?, NOW(), FALSE);", [
      req.body.userID,
      req.params.conversationID,
      req.body.content,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          console.log(err);
          return res.status(400).json({
                  msg: "Bad request",
          });
      }

      wss.verifyAndSendChatMessage(req.body.userID, results.insertId);

      return res.status(201).json({
              msg: "Message was created",
              id: results.insertId,
      });
  });
});

messagesRouter.put("/:conversationID/:messageID", verifyToken, tokenToId,
  checkConversationParticipation, (req: Request, res: Response) => {
  database.query("UPDATE messages set readByReceiver = TRUE WHERE id = ? AND conversationID = ?;", [
      req.params.messageID,
      req.params.conversationID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          console.log(err);
          return res.status(400).json({
                  msg: "Bad request",
          });
      }
      return res.status(204).json({
              msg: "Message was read",
      });
  });
});
