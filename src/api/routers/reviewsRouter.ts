import express, { Request, Response } from "express";
import jwt, { JsonWebTokenError } from "jsonwebtoken";
import { database } from "../../database/init";
import { verifyMentor } from "../../middleware/mentorVerification";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { verifyToken } from "../../middleware/tokenVerification";
import { INotification, sendNotificationToRoles } from "../../util/notificationsUtils";
import { verifyTutorshipHadSession } from "../../util/usersUtils";

export const reviewsRouter = express.Router();

reviewsRouter.post("/:tutorID", verifyToken, tokenToId,
  (req: Request, res: Response) => {
  verifyTutorshipHadSession(Number(req.params.tutorID), Number(req.userID), (ret: boolean) => {
    if (ret) {
      database.query("INSERT INTO reviews (tutorID, senderID, timestamp, communication, organization, content, extra) VALUES (?, ?, NOW(), ?, ?, ?, ?);", [
          req.params.tutorID,
          req.userID,
          req.body.communication,
          req.body.organization,
          req.body.content,
          req.body.extra,
      ], (err: any, results: any, fields: any) => {
          if (err) {
              console.log(err);
              return res.status(400).json({
                      msg: "Bad request",
              });
          } else {
            sendNotificationToRoles(["mentor"], {title: "New Review", content: "A tutee has made a review for a tutor", url: ""});

            return res.status(201).json({
                    msg: "Review was created",
            });
          }
      });
    } else {
      return res.status(403).json({
        msg: "User cannot write a review for this tutor",
      });
    }
  });
});

reviewsRouter.get("/:tutorID", verifyToken, tokenToId, verifyMentor,
  (req: Request, res: Response) => {
  database.query("SELECT r.*, CONCAT(u.firstname, ' ', u.lastname) AS tutorName FROM reviews r INNER JOIN users u ON u.id = r.tutorID WHERE r.tutorID = ?;", [
      req.params.tutorID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
                msg: "Bad request",
        });
      } else {
        const reviews = JSON.parse(JSON.stringify(results));
        return res.status(200).json({
                msg: "Reviews get",
                reviews,
        });
      }
  });
});

reviewsRouter.get("/", verifyToken, tokenToId, verifyMentor, (req: Request, res: Response) => {
  database.query("SELECT r.*, CONCAT(u.firstname, ' ', u.lastname) AS tutorName FROM reviews r INNER JOIN users u ON u.id = r.tutorID;", [
      req.params.tutorID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
                msg: "Bad request",
        });
      } else {
        const reviews = JSON.parse(JSON.stringify(results));
        return res.status(200).json({
                msg: "Reviews get",
                reviews,
        });
      }
  });
});
