import express, { Request, Response } from "express";
import jwt, { JsonWebTokenError } from "jsonwebtoken";
import { database } from "../../database/init";
import { wss } from "../../index";
import { checkConversationParticipation } from "../../middleware/conversationParticipantVerification";
import { verifyMentor } from "../../middleware/mentorVerification";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { verifyToken } from "../../middleware/tokenVerification";
import { sendNotificationToUsers } from "../../util/notificationsUtils";
import { getUsersByRoles } from "../../util/usersUtils";

export const notificationsRouter = express.Router();

notificationsRouter.get("/", verifyToken, tokenToId, (req: Request, res: Response) => {
  database.query("SELECT id, title, content, url, timestamp, readByReceiver FROM notifications WHERE receiverID = ?;", [
      req.body.userID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
                msg: "Bad request",
        });
      } else {
        const notifications = JSON.parse(JSON.stringify(results));
        return res.status(200).json({
                msg: "Notifications get",
                notifications,
        });
      }
  });
});

notificationsRouter.post("/", verifyToken, tokenToId, verifyMentor,
  (req: Request, res: Response) => {
  database.query("INSERT INTO notifications (receiverID, timestamp, title, content, url) VALUES (?, NOW(), ?, ?, ?);", [
      req.body.receiverID,
      req.body.title,
      req.body.content,
      req.body.url,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          console.log(err);
          return res.status(400).json({
                  msg: "Bad request",
          });
      } else {
        wss.verifyAndSendNotification(results.insertId);
        return res.status(201).json({
                msg: "Notification was created",
                id: results.insertId,
        });
      }
  });
});

notificationsRouter.put("/:notificationID", verifyToken, tokenToId,
  (req: Request, res: Response) => {
  database.query("UPDATE notifications set readByReceiver = TRUE WHERE id = ? AND receiverID = ?", [
      req.params.notificationID,
      req.body.userID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          console.log(err);
          return res.status(400).json({
                  msg: "Bad request",
          });
      } else {
        return res.status(204).json({
                msg: "Notification was read",
        });
      }
  });
});

notificationsRouter.delete("/:notificationID", verifyToken, tokenToId,
  (req: Request, res: Response) => {
  database.query("DELETE FROM notifications WHERE id = ? AND receiverID = ?", [
      req.params.notificationID,
      req.body.userID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          console.log(err);
          return res.status(400).json({
                  msg: "Bad request",
          });
      } else {
        return res.status(204).json({
                msg: "Notification was deleted",
        });
      }
  });
});

notificationsRouter.post("/multi", verifyToken, tokenToId, verifyMentor,
  (req: Request, res: Response) => {
  getUsersByRoles(req.body.roles, (users: any[]) => {
    if (users && users.length > 0) {
      sendNotificationToUsers({title: req.body.title, content: req.body.content, url: req.body.url}, users, () => {
        return res.status(201).json({
                msg: "Notifications were sent",
        });
      });
    } else {
      return res.status(404).json({
              msg: "No users with given role were found",
      });
    }
  });
});
