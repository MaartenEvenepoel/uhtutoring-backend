import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../../database/init";
import { tokenToId } from "../../middleware/requesterIdConversion";
import { getRole } from "../../middleware/roleRetrieval";
import { verifyToken } from "../../middleware/tokenVerification";
import { verifyTutor } from "../../middleware/tutorVerification";
import { INotification, sendNotification } from "../../util/notificationsUtils";

export const tutorshipsRouter = express.Router();

tutorshipsRouter.post("/", verifyToken, verifyTutor, tokenToId, (req: Request, res: Response) => {

    database.query("INSERT INTO tutorships (tutorID, tuteeID, advertisementID) VALUES (?, ?, ?)", [
        req.body.userID,
        req.body.tuteeID,
        req.body.advertisementID,
    ], (insertErr: MysqlError | null, results) => {

        if (insertErr) {
            return res.status(500).json({ msg: insertErr });
        } else {

            database.query("SELECT * FROM tutorships WHERE id=?", [results.insertId], (selectErr: MysqlError | null, rows: any) => {

                if (selectErr) {
                    return res.status(500).json({ msg: insertErr });
                }
                sendNotification({receiverID: req.body.tuteeID, title: "Advertisement accepted", content: "A tutor has accepted your tutoring advertisement", url: ""});

                const data = JSON.parse(JSON.stringify(rows[0]));

                return res.status(201).json({
                    msg: "Succesfully created tutorship",
                    data,
                });
            });
        }
    });

});

tutorshipsRouter.get("/", verifyToken, tokenToId, getRole, (req: Request, res: Response) => {

    if (req.body.role === "mentor") {

        database.query("SELECT t.id, t.tutorID, u1.firstname AS tutorFirstName, u1.lastname AS tutorLastName, t.tuteeID, u2.firstname AS tuteeFirstName, u2.lastname AS tuteeLastName, t.advertisementID, t.creationTimestamp, t.updateTimestamp FROM tutorships t"
        + " INNER JOIN users u1 ON t.tutorID = u1.id"
        + " INNER JOIN users u2 ON t.tuteeID = u2.id", (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: err });
            } else {
                const data = JSON.parse(JSON.stringify(results));

                return res.status(200).json({
                    msg: "Succesfully queried tutorships",
                    data,
                });
            }
        });

    } else {

        database.query("SELECT t.id, t.tutorID, u1.firstname AS tutorFirstName, u1.lastname AS tutorLastName, t.tuteeID, u2.firstname AS tuteeFirstName, u2.lastname AS tuteeLastName, t.advertisementID, t.creationTimestamp, t.updateTimestamp FROM tutorships t"
        + " INNER JOIN users u1 ON t.tutorID = u1.id"
        + " INNER JOIN users u2 ON t.tuteeID = u2.id WHERE t.tuteeID=? OR t.tutorID=?", [
            req.body.userID,
            req.body.userID,
        ], (err: MysqlError | null, results: any) => {
            if (err) {
                return res.status(500).json({ msg: err });
            } else {
                const data = JSON.parse(JSON.stringify(results));

                return res.status(200).json({
                    msg: "Succesfully queried tutorships",
                    data,
                });
            }
        });

    }

});

tutorshipsRouter.delete("/:id", verifyToken, tokenToId, (req: Request, res: Response) => {

    database.query("DELETE tutorships WHERE id=? AND (tutorID=? OR tuteeID=?)", [
        req.params.id,
        req.body.userID,
        req.body.userID,
    ], (err: MysqlError | null, results: any) => {
        if (err) {
            return res.status(500).json({ msg: err });
        } else {
            return res.status(200).json({
                msg: "Succesfull delete",
                data: {
                    recordsAffected: results.affectedRows,
                },
            });
        }
    });

});
