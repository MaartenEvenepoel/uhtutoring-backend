import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import logger from "../middleware/logger";
import { advertisementsRouter } from "./routers/advertisementsRouter";
import { healthRouter } from "./routers/healthRouter";
import { messagesRouter } from "./routers/messagesRouter";
import { notificationsRouter } from "./routers/notificationsRouter";
import { programmesRouter } from "./routers/programmesRouter";
import { reviewsRouter } from "./routers/reviewsRouter";
import { sessionsRouter } from "./routers/sessionsRouter";
import { tutorshipsRouter } from "./routers/tutorshipsRouter";
import { usersRouter } from "./routers/usersRouter";

export const app = express()

    // Middleware
    .use(bodyParser.json())
    .use(cors())
    .use(logger)

    // Routers
    .use("/programmes", programmesRouter)
    .use("/sessions", sessionsRouter)
    .use("/tutorships", tutorshipsRouter)
    .use("/applications", advertisementsRouter)
    .use("/health", healthRouter)
    .use("/users", usersRouter)
    .use("/messages", messagesRouter)
    .use("/reviews", reviewsRouter)
    .use("/notifications", notificationsRouter);
