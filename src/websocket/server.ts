import * as WebSocket from "ws";
import { database } from "../database/init";
import { IMessage, verifyConversationParticipant, verifyMessage } from "../util/messagesUtils";
import { INotification, verifyNotification } from "../util/notificationsUtils";
import { getUsersByRoles, tokenToUser } from "../util/usersUtils";

/*
* Usage of this websocket server:
* A user needs to send a message containting the "subscribe" type and a conversationID
* in order to register for future messages in this conversation.
*
* Example subscription message:
* { "type": "subscribe-conversation", "conversationID": 1, "token": "fdsalkjflsadf" }
* { "type": "subscribe-notifications", "token": "fdsalkjflsadf" }
*
*/

export class WebSocketServer {
  private conversationMap: Map<number, WebSocket[]> = new Map<number, WebSocket[]>();
  private userMap: Map<WebSocket, number> = new Map<WebSocket, number>();
  private notificationMap: Map<number, WebSocket> = new Map<number, WebSocket>();

  constructor(server: any) {
    const wss = new WebSocket.Server({ server });
    wss.on("connection", (ws: any) => {
      ws.on("message", (message: string) => {
        try {
          const data = JSON.parse(message);
          if (data.token !== undefined && data.type !== undefined) {
            tokenToUser(data.token, (userID: number, name: string) => {
              if (data.type === "subscribe" || data.type === "subscribe-conversation") {
                verifyConversationParticipant(userID, data.conversationID, () => {
                  this.addSocketToConversation(ws, data.conversationID, userID);
                });
              } else if (data.type === "subscribe-notifications") {
                this.notificationMap.set(userID, ws);
              }
            });
          }
        } catch (e) {
          console.log("websocket message not valid json");
        }
      });
    });
  }

  public verifyAndSendChatMessage(userID: number, messageID: number): void {
    verifyMessage(userID, messageID, (m: IMessage) => {
        this.sendMessageToConversation(m);
    });
  }
  public verifyAndSendNotification(notificationID: number): void {
    verifyNotification(notificationID, (n: INotification) => {
      this.sendNotification(n);
    });
  }

  private addSocketToConversation(ws: WebSocket, conversationID: number, userID: number): void {
    const oldarr = this.conversationMap.get(conversationID);
    if (oldarr !== undefined) {
      if (!oldarr.includes(ws)) {
        oldarr.push(ws);
        this.conversationMap.set(conversationID, oldarr);
      }
    } else {
      const newarr: WebSocket[] = [];
      newarr.push(ws);
      this.conversationMap.set(conversationID, newarr);
    }
    this.userMap.set(ws, userID);
  }

  private sendMessageToConversation(m: IMessage): void {
    if (m.conversationID) {
      const sockets = this.conversationMap.get(m.conversationID);
      if (sockets) {
        for (const socket of sockets) {
          const sentByCurrentUser = +(this.userMap.get(socket) === m.senderID);
          socket.send(JSON.stringify({
            id: m.id,
            content: escape(m.content),
            timestamp: m.timestamp,
            sender: escape(m.senderName),
            readByReceiver: false,
            sentByCurrentUser,
          }));
        }
      }
    }
  }
  private sendNotification(n: INotification): void {
    if (n.receiverID) {
      const socket = this.notificationMap.get(n.receiverID);
      if (socket) {
        socket.send(JSON.stringify({
          id: n.id,
          title: escape(n.title),
          content: escape(n.content),
          url: escape(n.url),
          timestamp: n.timestamp,
          readByReceiver: false,
        }));
      }
    }
  }
}
