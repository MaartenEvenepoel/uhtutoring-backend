import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../database/init";

interface IMessage {
  conversationID: number;
  content: string;
  timestamp: string;
  senderName: string;
  senderID?: number;
  id?: number;
}

/*
* Deletes a conversation
* @param conversationID conversation to delete
*/
function deleteConversation(conversationID: number): void {
  database.query("DELETE FROM conversations WHERE id = ?;", [ conversationID ],
      (err: any, results: any, fields: any) => {
      if (err) {
        console.log(err);
      }
  });
}

/*
* Creates a conversation and gets the id
* @param res: response for setting errors
* @param callback: function to execute afterward with id as param
*/

function createConversation(res: Response, callback: (conversationID: number) => any): void {
  database.query("INSERT INTO conversations (name) VALUES (NULL);", [], (err: any, results: any, fields: any) => {
    if (err) {
      return res.status(400).json({
              msg: "Bad request",
      });
    }
    callback(results.insertId);
  });
}

/*
* Add a user to a conversation
* @param res: response for setting errors
* @param conversationID: id of the conversation
* @param userID: id of the user
* @param callback: function to execute afterward
*/

function addUserToConversation(res: Response, conversationID: number, userID: number, callback: () => any): void {
  database.query("INSERT INTO conversationParticipants (conversationID, userID) VALUES (?, ?);", [
      conversationID,
      userID,
    ], (err: any, results: any, fields: any) => {
    if (err) {
      deleteConversation(conversationID);
      return res.status(400).json({
              msg: "A specified user does not exist",
      });
    } else {
      callback();
    }
  });
}

/*
* Creates the tutor group conversation
* @param res: response for setting errors
* @param callback: function to execute afterward with created conversation id
*/

function createTutorConversation(res: Response, callback: (conversationID: number) => any): void {
  database.query("INSERT INTO conversations (name) VALUES ('Tutors');",
  (err: MysqlError | null, results: any) => {
    if (err) {
      return res.status(500).json({ msg: err });
    } else {
      const conversationID = results.insertId;
      database.query("INSERT INTO tutorConversation (id) VALUES (?);", [
        conversationID,
      ], (err2: MysqlError | null, results2: any) => {
        if (err2) {
          return res.status(500).json({ msg: err });
        } else {
          callback(conversationID);
        }
      });
    }
  });
}

/*
* Checks if the user is a participant in a conversation
* @param userID: id of the user
* @param conversationID: id of the conversation
* @param callback: function to execute afterwards if the user is a participant
*/

function verifyConversationParticipant(userID: number, conversationID: number, callback: () => any): void {
  database.query("SELECT * FROM conversationParticipants WHERE conversationID = ? AND userID = ?;", [
      conversationID,
      userID,
  ], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      return callback();
    }
  });
}

/*
* Adds a user to the tutor group conversation
* @param res: response for setting errors
* @param userID: id of user to add
* @param callback: function to execute afterward
*/

function addUserToTutorConversation(res: Response, userID: number, callback: () => any): void {
  database.query("SELECT * FROM tutorConversation LIMIT 1", (err: MysqlError | null, results: any) => {
    if (err) {
      return res.status(500).json({ msg: err });
    } else if (!results || results.length === 0) {
      createTutorConversation(res, (conversationID: number) => {
        addUserToConversation(res, conversationID, userID, () => {
          callback();
        });
      });
    } else {
      addUserToConversation(res, results[0].id, userID, () => {
        callback();
      });
    }
  });
}

/*
* Checks if the message was sent by the specified user and makes sure it is correct
* @param userID: id of the user
* @param messageID: id of the message
* @param callback: function to execute afterwards
*/

function verifyMessage(userID: number, messageID: number, callback: (m: IMessage) => any): void {
  database.query("SELECT u.firstname AS name, m.conversationID, m.content, m.timestamp FROM messages m INNER JOIN users u ON u.id = m.senderID WHERE m.id = ? AND m.senderID = ?;", [
      messageID,
      userID,
  ], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      return callback({conversationID: results[0].conversationID, content: results[0].content, timestamp: results[0].timestamp, senderName: results[0].name, senderID: userID, id: messageID});
    }
  });
}

export { createConversation, addUserToConversation, addUserToTutorConversation, verifyConversationParticipant, verifyMessage, IMessage };
