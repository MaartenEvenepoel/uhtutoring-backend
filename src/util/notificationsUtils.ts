import express, { Request, Response } from "express";
import { MysqlError } from "mysql";
import { database } from "../database/init";
import { wss } from "../index";
import { getUsersByRoles } from "./usersUtils";

interface INotification {
  title: string;
  content: string;
  url: string;
  receiverID?: number;
  timestamp?: string;
  id?: number;
}

/*
* Creates a new notification in the database and sends a socket message
* @param n: notification to create
*/

function sendNotification(n: INotification): void {
  database.query("INSERT INTO notifications (receiverID, timestamp, title, content, url) VALUES (?, NOW(), ?, ?, ?);", [
    n.receiverID,
    n.title,
    n.content,
    n.url,
  ], (err: any, results: any, fields: any) => {
    if (!err && results !== undefined) {
      wss.verifyAndSendNotification(results.insertId);
    }
  });
}

/*
* Creates a new notification for multiple users
* @param n: notification content, title and url
* @param users: array of database users
* @param callback: optional function to execute afterward
*/

function sendNotificationToUsers(n: INotification, users: any[], callback: (() => any) | null): void {
  sendNotification({receiverID: users[0].id, title: n.title, content: n.content, url: n.url});
  users.shift();
  if (users.length > 0) {
    sendNotificationToUsers(n, users, callback);
  } else {
    if (callback) {
      callback();
    }
  }
}

/*
* Makes sure the notification is correct
* @param userID: id of the user
* @param notificationID: id of the notification
* @param callback: function to execute afterwards
*/

function verifyNotification(notificationID: number, callback: (n: INotification) => any): void {
  database.query("SELECT title, content, url, receiverID, timestamp FROM notifications WHERE id = ?;", [
      notificationID,
  ], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      return callback({id: notificationID, title: results[0].title, content: results[0].content, url: results[0].url, receiverID: results[0].receiverID, timestamp: results[0].timestamp});
    }
  });
}

/*
* Sends a notification to all users with one of the given roles
* @param roles: array containing the roles
* @param n: notification content, title and url
*/

function sendNotificationToRoles(roles: string[], n: INotification): void {
  getUsersByRoles(roles, (users: any[]) => {
    if (users && users.length > 0) {
      sendNotificationToUsers(n, users, null);
    }
  });
}

/*
* Checks if there are advertisments open for more than 2 days and sends notifications to tutors if that is the case
*/

function sendNotificationForOpenAdvertisements(): void {
  database.query("SELECT * FROM advertisements WHERE id NOT IN (SELECT advertisementID from tutorships) AND creationTimestamp < DATE_SUB(NOW(), INTERVAL 2 DAY);", [], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      sendNotificationToRoles(["tutor"], {title: "Tutors needed", content: "There are advertisements that have been open for more than two days.", url: "Advertisements"});
    }
  });
}

export { sendNotificationToUsers, verifyNotification, sendNotificationForOpenAdvertisements, sendNotificationToRoles, INotification, sendNotification };
