import { mailTemplate } from "../mail/declarations.ts/mailTemplate";
import { generateFilledTemplate } from "../mail/templateLoader";

async function buildConfirmationURL(confirmationHash: string) {
    return `${process.env.WEBSERVER_HTTP_PREFIX}${process.env.WEBSERVER}ConfirmEmail?hash=${confirmationHash}`;
}

export async function buildConfirmationMailContent(userData: object, registrationHash: string) {
    const confirmationURL: string = await buildConfirmationURL(registrationHash);
    const maildata = {
        ...userData,
        confirmationURL,
    };
    return await generateFilledTemplate(mailTemplate.emailconfirmation, maildata);
}

export async function buildRejectionMailContent(userData: object, message: string) {
    const mailData = {
        ...userData,
        message,
    };
    return await generateFilledTemplate(mailTemplate.tutorRejection, mailData);
}

export async function buildAcceptanceMailContent(userData: object, message: string) {
    const maildata = {
        ...userData,
        message,
    };
    return await generateFilledTemplate(mailTemplate.tutorAcceptance, maildata);
}

export async function buildMailContent(userData: object, template: mailTemplate) {
    const maildata = {
        ...userData,
    };
    return await generateFilledTemplate(template, maildata);
}
