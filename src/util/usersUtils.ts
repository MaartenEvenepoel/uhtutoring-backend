import { Response } from "express";
import hasha from "hasha";
import { MysqlError } from "mysql";
import { database } from "../database/init";

/*
* Gets tutee and tutor id from tutorship
* @param res: response for setting errors
* @param tutorshipID: id of tutorship
* @param callback: function to execute afterwards with ids as params
*/

function getUsersFromTutorship(res: Response, tutorshipID: number, callback: (tuteeID: number, tutorID: number) => any): void {
  database.query("SELECT tutorID, tuteeID FROM tutorships WHERE id = ?;", [
      tutorshipID,
  ], (err: any, results: any, fields: any) => {
      if (err) {
          return res.status(400).json({
                  msg: "Bad request",
          });
      } else if (results.length === 0) {
          return res.status(403).json({
                  msg: "Invalid tutorship",
          });
      } else {
          const tutorID = results[0].tutorID;
          const tuteeID = results[0].tuteeID;
          callback(tutorID, tuteeID);
      }
  });
}

/*
* Helper function for next function
*/

function helpGetUsersByRoles(roles: string[], users: any[], callback: (results: any[]) => any): void {
  database.query("SELECT * FROM users WHERE role = ?", [
    roles[0].toLowerCase(),
  ], (err: any, results: any, fields: any) => {
        if (err) {
            console.log(err);
        } else {
            roles.shift();
            if (roles.length > 0) {
                helpGetUsersByRoles(roles, users.concat(results), callback);
            } else {
                callback(users.concat(results));
            }
        }
    });
}

/*
* Returns all user data from users with one of the given roles.
* @param roles: array containing the roles as string
* @param callback: function to execute after this one is done
*/

function getUsersByRoles(roles: string[], callback: (results: any[]) => any): void {
    helpGetUsersByRoles(roles, [], callback);
}

/*
* Takes a token and converts it to a userID
* @param token: user token
* @param callback: function to execute afterward
*/

function tokenToUser(token: string, callback: (userID: number, name: string) => any): void {
    database.query("SELECT users.id AS userID, users.firstname AS name FROM users INNER JOIN userTokens ON users.id = userTokens.userID WHERE userTokens.token = ?;", [
      token,
    ], (err: MysqlError | null, results: any) => {
        if (!err && results !== undefined && results.length > 0) {
            return callback(results[0].userID, results[0].name);
        }
    });
}

/*
* Verifies that the userID belongs to a mentor user
* @param userID: user id
* @param callback: function to execute afterward if the user is a mentor
*/
function verifyMentor(userID: number, callback: () => any): void {
    database.query("SELECT role FROM users WHERE id = ?;", [
        userID,
    ], (err: MysqlError | null, results: any) => {
        if (!err && results !== undefined && results.length > 0) {
            if (results[0].role === "mentor" || results[0].role === "admin") {
                return callback();
            }
        }
    });
}

async function createAndStoreRegistrationHash(email: string, message: string) {
    const hash: string = await hasha.async(message);
    database.query("INSERT INTO registrationHashes (email, hash) VALUES (?,?)", [
        email,
        hash,
    ], (err: MysqlError | null) => {
        if (err) {
            throw err;
        }
    });
    return hash;
}

/*
* Retreives the tutorship id based on tutee and tutor id
* @param tutorID: id of the tutor
* @param tuteeID: id of the tutee
* @param callback: function to execute afterward with the resulting id
*/

function getTutorshipByUserIDs(tutorID: number, tuteeID: number, callback: (tutorshipID: number|null) => any) {
  database.query("SELECT id FROM tutorships WHERE tutorID = ? AND tuteeID = ?;", [
      tutorID,
      tuteeID,
  ], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      callback(results[0].id);
    } else {
      callback(null);
    }
  });
}

/*
* Verifies that there has been a scheduled session within the tutorship before
* @param tutorshipID: id of the tutorship
* @param callback: function to execute afterward with the result as a boolean
*/

function verifySessionBeforeNow(tutorshipID: number, callback: (ret: boolean) => any) {
  database.query("SELECT * FROM sessions WHERE tutorshipID = ? AND date < NOW();", [
      tutorshipID,
  ], (err: MysqlError | null, results: any) => {
    if (!err && results !== undefined && results.length > 0) {
      callback(true);
    } else {
      callback(false);
    }
  });
}

/*
* Verifies that the tutee and tutor have a tutorship and have had a session before this date
* @param tutorID: id of the tutor
* @param tuteeID: id of the tutee
* @param callback: function to execute afterward with the result as a boolean
*/

function verifyTutorshipHadSession(tutorID: number, tuteeID: number, callback: (ret: boolean) => any) {
  getTutorshipByUserIDs(tutorID, tuteeID, (tutorshipID: number|null) => {
    if (tutorshipID) {
      verifySessionBeforeNow(tutorshipID, (ret: boolean) => { callback(ret); });
    } else {
      callback(false);
    }
  });
}

export { getUsersByRoles, getUsersFromTutorship, tokenToUser, verifyMentor, verifyTutorshipHadSession, createAndStoreRegistrationHash };
