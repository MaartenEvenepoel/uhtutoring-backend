export enum mailTemplate {
    emailconfirmation = "emailconfirmation",
    tutorAcceptance = "tutorAcceptance",
    tutorRejection = "tutorRejection",
}
