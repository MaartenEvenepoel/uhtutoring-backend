import fs from "fs";
import Handlebars from "handlebars";
import { mailTemplate } from "./declarations.ts/mailTemplate";

/**
 * Loads a template as string from a file (utf-8)
 * @param template The template that must be opened
 */
async function loadTemplateFromFile(template: mailTemplate) {
    return fs.readFileSync(`./src/mail/templates/${template}.html`, "utf-8");
}

/**
 * Fills template with given data
 * @param template The template that must be used
 * @param content Variable data used in the template
 */
async function loadConfirmationTemplate(template: mailTemplate, content: any) {
    const templateCompiled = Handlebars.compile((await loadTemplateFromFile(template)));

    const url: string = content.confirmationURL;

    Handlebars.registerHelper("confirmationlink", () => {
        const link: string = `<a href="${url}">Klik hier</a>`;

        return new Handlebars.SafeString(link);
    });

    return templateCompiled(content);
}

/**
 * Fills template with given data
 * @param template The template that must be used
 * @param content Variable data used in the template
 */
async function loadAcceptanceTemplate(template: mailTemplate, content: any) {
    const templateCompiled = Handlebars.compile((await loadTemplateFromFile(template)));
    return templateCompiled(content);
}

async function loadRejectionTemplate(template: mailTemplate, content: any) {
    const templateCompiled = Handlebars.compile((await loadTemplateFromFile(template)));
    return templateCompiled(content);
}

/**
 * Generates a given template with given content
 * @param template The template that must be used
 * @param content Variable data used in the template
 */
export async function generateFilledTemplate(template: mailTemplate, content: object) {
    switch (template) {
        case mailTemplate.emailconfirmation:
            return loadConfirmationTemplate(template, content);
        case mailTemplate.tutorAcceptance:
            return loadAcceptanceTemplate(template, content);
        case mailTemplate.tutorRejection:
            return loadRejectionTemplate(template, content);
    }
}
