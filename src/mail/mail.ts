import nodemailer, { Transporter } from "nodemailer";
import { mailTemplate } from "./declarations.ts/mailTemplate";

let transporter: Transporter;

/**
 * Send a mail according to the given mail object
 * @param mail json object with fields 'from', 'to', 'subject', 'text', 'html'
 * @param type type of email that must be sent
 * @param content content of the mail that must be filled in the template
 */
export async function sendMail(recipient: string, template: mailTemplate, content: string) {

    if (transporter === undefined) {
        // create reusable transporter object using the default SMTP transport
        transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: Number(process.env.SMTP_PORT),
            secure: Boolean(process.env.SMTP_SECURE),
            auth: {
                user: process.env.SMTP_USER,
                pass: process.env.SMTP_PASS,
            },
        });
    }

    let mail: object = {
        from: "UHasselt Tutoring",
        to: recipient,
        html: content,
    };

    if (template === mailTemplate.emailconfirmation) {
        mail = {
            ...mail,
            subject: "Bevestig je account",
        };
    } else if (template === mailTemplate.tutorAcceptance) {
        mail = {
            ...mail,
            subject: "Welkom tot UHasselt Tutoring!",
        };
    } else if (template === mailTemplate.tutorRejection) {
        mail = {
            ...mail,
            subject: "Je aanvraag als tutor",
        };
    }

    await transporter.sendMail({
        ...mail,
    });
}
