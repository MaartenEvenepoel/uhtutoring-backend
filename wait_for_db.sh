#!/bin/bash
# will keep running until database has been fully initialized
sudo docker exec -it mysql-node /bin/bash -c "mysql -proot -e'use root; select * from users;'" > /dev/null @>&1
while [ $? != 0 ]; do
   sleep 3
   sudo docker exec -it mysql-node /bin/bash -c "mysql -proot -e'use root; select * from users;'" > /dev/null @>&1
done
