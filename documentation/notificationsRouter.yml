openapi: "3.0.0"

info:
  version: 1.0.0
  title: Notifications Router

paths:
  /notifications:
    get:
      summary: "Get all notifications for a user"
      tags:
        - Notifications
      responses:
        '200':
          description: "Succesfully returned notifications"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/notification"
        '401':
          description: "User is not authenticated"
    post:
      summary: "Create a notification"
      tags:
        - Notifications
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/postnotification'
      responses:
        '201':
          description: "Succesfully created notification"
          content:
            application/json:
              schema:
                type: "object"
                properties:
                  id:
                    type: "integer"
                    example: 1
        '401':
          description: "User is not allowed to post this notification"
  /notifications/{notificationID}:
    put:
      summary: "Read a notification"
      tags:
        - Notifications
      parameters:
          - in: "path"
            name: "notificationID"
            schema:
              type: "integer"
            required: true
            description: "The id of the notification to read"
      responses:
        '201':
          description: "Succesfully read notification"
        '401':
          description: "User is not allowed to read this notification"
    delete:
      summary: "Delete a notification"
      tags:
        - Notifications
      parameters:
          - in: "path"
            name: "notificationID"
            schema:
              type: "integer"
            required: true
            description: "The id of the notification to delete"
      responses:
        '201':
          description: "Succesfully deleted notification"
        '401':
          description: "User is not allowed to delete this notification"
  /notifications/multi:
    post:
      summary: "Create a notification for all users with certain roles"
      tags:
        - Notifications
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/multinotification'
      responses:
        '201':
          description: "Succesfully created notifications"
          content:
            application/json:
              schema:
                type: "object"
                properties:
                  notificationIDs:
                    type: "array"
                    items:
                      type: "integer"
                      example: 1
        '401':
          description: "User is not allowed to post these notifications"
        '404':
          description: "No users with given role were found"

components:
  schemas:
    notification:
      type: "object"
      properties:
        id:
          type: "integer"
          example: 1
        title:
          type: "string"
          example: "notificatie"
        content:
          type: "string"
          example: "hallo"
        url:
          type: "string"
          example: "https://www.uhasselt.be"
        timestamp:
          type: "string"
          example: "2019-11-11T01:24:55.000Z"
        read:
          type: "boolean"
          example: false
      required:
        - "id"
        - "title"
        - "read"
        - "timestamp"
    postnotification:
      type: "object"
      properties:
        receiverID:
          type: "integer"
          example: 1
        title:
          type: "string"
          example: "notificatie"
        content:
          type: "string"
          example: "hallo"
        url:
          type: "string"
          example: "https://www.uhasselt.be"
      required:
        - "receiverID"
        - "title"
    multinotification:
      type: "object"
      properties:
        roles:
          type: "array"
          items:
            type: "string"
            example: "tutee"
        title:
          type: "string"
          example: "notificatie"
        content:
          type: "string"
          example: "hallo"
        url:
          type: "string"
          example: "https://www.uhasselt.be"
      required:
        - "receiverID"
        - "title"
