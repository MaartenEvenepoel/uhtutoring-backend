import chai from "chai";
import chaiHttp from "chai-http";
import "mocha";
import { app } from "../src/api";

chai.use(chaiHttp);

// describe("POST", () => {
//     it("should succesfully create a new advertisement", async () => {
//         return chai.request(app)
//         .post("/advertisements")
//         .set({
//             Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
//         })
//         .send({
//             title: "Bijles CCS",
//             description: "Ik wil graag bijles over CCS.",
//         }).then((res) => {
//             chai.expect(res.status).to.equal(201);
//         });
//     });
// });
