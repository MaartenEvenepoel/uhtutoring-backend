import chai from "chai";
import chaiHttp from "chai-http";
import { Response } from "express";
import "mocha";
import { app } from "../src/api";

chai.use(chaiHttp);

describe("Messages test", () => {
  describe("GET", () => {
          it("should return a list of messages for the conversation", async () => {
              return chai.request(app).get("/messages/get/1")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(200);
                      chai.expect(res).to.be.json;
                      chai.expect(res.body).to.have.property("messages");
                      chai.expect(res.body.messages).to.be.an("array");
                });
          });
          it("should return 401 because unauthorized", async () => {
              return chai.request(app).get("/messages/get/1")
                .then((res) => {
                      chai.expect(res).to.have.status(401);
                });
          });
  });
  describe("POST", () => {
          it("should create a new message and return id", async () => {
              return chai.request(app).post("/messages/send/1")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .send({
                    content: "Test bericht",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(201);
                      chai.expect(res.body).to.have.property("id");
                });
          });
  });
  describe("PUT", () => {
          it("should update the readByReceiver status of a message to true", async () => {
              return chai.request(app).put("/messages/read/1/1")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(204);
                });
          });
  });
  describe("GET conversations", () => {
          it("should get a list of every conversation the user is a part of", async () => {
              return chai.request(app).get("/messages/conversations")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(200);
                      chai.expect(res).to.be.json;
                      chai.expect(res.body).to.have.property("conversations");
                      chai.expect(res.body.conversations).to.be.an("array");
                });
          });
  });
  describe("POST conversation", () => {
          it("should create a new conversation based on tutorshipID and return id", async () => {
              return chai.request(app).post("/messages/conversation/3")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(200);
                      chai.expect(res).to.be.json;
                      chai.expect(res.body).to.have.property("id");
                });
          });
  });
});
