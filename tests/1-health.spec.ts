import chai from "chai";
import chaiHttp from "chai-http";
import { Response } from "express";
import "mocha";
import { app } from "../src/api";
import { initDatabase } from "../src/database/init";

chai.use(chaiHttp);
initDatabase();

describe("Health test", () => {
  it("should GET status 200", async () => {
      return chai.request(app).get("/health")
        .then((res) => {
              chai.expect(res).to.have.status(200);
        });
  });
});
