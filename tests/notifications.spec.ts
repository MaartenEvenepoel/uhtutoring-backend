import chai from "chai";
import chaiHttp from "chai-http";
import { Response } from "express";
import "mocha";
import { app } from "../src/api";
// import { initDatabase } from "../src/database/init";

chai.use(chaiHttp);
// initDatabase();

describe("Notifications", () => {
  describe("POST", () => {
          it("should create a new notification", async () => {
              return chai.request(app).post("/notifications/send")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .send({
                    receiverID: 1,
                    title: "Test melding",
                    content: "Test content",
                    url: "uhasselt.be",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(201);
                      chai.expect(res.body).to.have.property("id");
                });
          });
  });
  describe("GET", () => {
          it("should return a list of notifications for the user", async () => {
              return chai.request(app).get("/notifications")
                .set({
                    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdG5hbWUiOiJSb25ueSIsImxhc3RuYW1lIjoiRHJvbWVkYXJpcyIsImVtYWlsIjoiZGV2LmRldkBzdHVkZW50LnVoYXNzZWx0LmJlIiwicGFzc0hhc2giOiJzaGExJDkwMzZhYWU2JDEkYTMyZmNmYTdmMGIxNzNhMmEwN2EzMzJkZjYwY2EwM2ZhNDg1OGFkYyIsInJvbGUiOiJ0dXRvciJ9LCJpYXQiOjE1NzM5MjAzMzJ9.xF4SbkwJz422Fak1Sg0RzwUviZFnca8bvhJEAcaaBNs",
                })
                .then((res) => {
                      chai.expect(res).to.have.status(200);
                      chai.expect(res).to.be.json;
                      chai.expect(res.body).to.have.property("notifications");
                      chai.expect(res.body.notifications).to.be.an("array");
                });
          });
          it("should return 401 because unauthorized", async () => {
              return chai.request(app).get("/notifications")
                .then((res) => {
                      chai.expect(res).to.have.status(401);
                });
          });
  });
});
