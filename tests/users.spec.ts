import chai from "chai";
import chaiHttp from "chai-http";
import { Response } from "express";
import "mocha";
import { app } from "../src/api";

chai.use(chaiHttp);

// // Register tutee
// describe("Register tutee", () => {
//     it("should succesfully register tutee and respond with code 200", async () => {
//         return chai.request(app).post("/users/register").send({
//             firstname: "John",
//             lastname: "Doe",
//             email: "john.doe@student.uhasselt.be",
//             password: "secretpassword",
//             role: "tutee",
//         }).then((res) => {
//         chai.expect(res.status).to.equal(200);
//         });
//     });
// });

// // Register tutor
// describe("Register tutor", () => {
//     it("should succesfully register tutor and respond with code 200", async () => {
//         return chai.request(app).post("/users/register")
//         .set("content-type", "application/json")
//         .send({
//             firstname: "Mike",
//             lastname: "Doe",
//             email: "mike.doe@student.uhasselt.be",
//             password: "secretpassword",
//             role: "tutor",
//         }).then((res) => {
//         chai.expect(res.status).to.equal(200);
//         });
//     });
// });
